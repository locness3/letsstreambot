// Base class for creating commands.
class BaseCommand {
  // Accepts a whole command with args, return an array with the command + these args.
  getCommandAsArray(command) {
    return command.split(" ");
  }

  // Return the command's name with prefix
  getCommandName(command) {
    return this.getCommandAsArray(command)[0];
  }

  // Returns only the args from that command. Doesn't accomodate the case where prefix has a space after it
  getArgs(command) {
    var commandArray = this.getCommandAsArray(command);
    commandArray.shift();
    return commandArray;
  }

  getRawArgs(command) {
    var argsArray = this.getArgs(command);
    return argsArray.join(" ");
  }

  constructor(client, commandName) {
    // commandName should be the whole name with the prefix

    this.client = client;
    this.commandName = commandName;

    var self = this;
    this.client.on("message", async function (msg) {
      if (self.getCommandName(msg.content).startsWith(self.commandName)) {
        await self.commandCallHandler(msg);
      }
    });
  }
}

module.exports = BaseCommand;
