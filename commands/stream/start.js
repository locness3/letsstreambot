const BaseCommand = require('../../basecommand.js');

module.exports = class StartStreamCommand extends BaseCommand {

    constructor(client, commandName, db) {
        super(client, commandName);
        this.db = db;
    }

    async commandCallHandler(msg) {
        var query = await this.db.get(`SELECT * FROM streamer_boards
            WHERE discord_cp_channel_id = ?`,
        msg.channel.id);

        if (!query) {
            msg.channel.send(`Tu dois être dans le\
            control panel de ton streamer board pour faire cela.`);
        }
        else {
            if (msg.author.id != query.discord_owner_id) {
                msg.channel.send("Tu n'es pas autorisé à faire cela ici.");
            }
            else {
                try {
                    // TODO : Modifier le même message au lieu d'en envoyer plusieurs
                    msg.channel.send("Démarrage du stream...");
                    await this.db.exec(`UPDATE streamer_boards
                        SET streaming_status = 1
                        WHERE discord_cp_channel_id = ?
                    `, msg.channel.id);
                    
                    msg.channel.send("Ouverture de votre salon de stream...");
                    var streamChannelId = query.discord_stream_channel_id;
                    var streamChannel = await this.client.channels.fetch(streamChannelId);
                    streamChannel.updateOverwrite(streamChannel.guild.roles.everyone, {VIEW_CHANNEL: true, CONNECT: true});

                    msg.channel.send("Notification des abonnés...")
                    var notificationsChannel = this.client.channels.cache.get("745286036808138823");
                    await notificationsChannel.send(`<@${query.discord_owner_id}> a démarré le stream.`);

                    msg.channel.send(":white_check_mark: Stream démarré! :tada:")
                }
                catch {
                    msg.channel.send(":x: Une erreur est survenue.")
                }
            }
        }
    }
}