const Discord = require("discord.js");
const sqlite3 = require("sqlite3");
const sqlite = require("sqlite");

const client = new Discord.Client();
client.login("TOKEN_REDACTED");

var db, clientCommands = [];

sqlite.open({
  filename: "./database.db",
  driver: sqlite3.Database
}).then((database) => {
  db = database;

  const StartStreamCommand = require("./commands/stream/start.js");
  clientCommands.push(new StartStreamCommand(client, "$start", db));
})